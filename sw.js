const cacheName = "galerie";

const files = [
  "/",
  "/script.js",
  "https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.css",
  "https://bulma.io/images/placeholders/1280x960.png",
  "https://bulma.io/images/placeholders/96x96.png"
];

self.addEventListener("install", e => {
    // a l'installation du sw on met les fichiers en cache
  caches.open(cacheName).then(cache => {
    cache.addAll(files);
  });
});
self.addEventListener("activate", e => {
    // a l'activation du sw on nettoie le cache des clefs non présente
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(
        keyList.map(function(key) {
          if (key !== cacheName) {
            return caches.delete(key);
          }
        })
      );
    })
  );
});
// A l'envoie d'une requete on affiche son url en console
self.addEventListener("fetch", e => {console.log(e.request.url);});

self.addEventListener("fetch", event => {
  const url = event.request.url;

  if (url.indexOf("https://priceless-meninsky-ffdf51.netlify.app/images.json") === 0) {
      //Formattage du json
    event.respondWith(
      fetch(event.request).then(response => {
        if (response.status === 200) {
			console.info("Formatting data");
			return response.json().then(json => {
			const formattedResponse = json.map(j => ({
			name: j.name,
			description: j.description || "",
			updated_at: j.updated_at
			}));

			return new Response(JSON.stringify(formattedResponse));
			});
        }
		
		else{
			console.error(
			"Service Worker",
			"Error when fetching",			
			event.request.url
			);

			return response;
		}
        
      })
    );
  } 
  else {
      //On enregistre les autres requete que celle vers images.json dans le cache
    event.respondWith(
      caches
        .open(cacheName)
        .then(cache => cache.match(event.request))
        .then(response => response || fetch(event.request))
    );
  }
});
